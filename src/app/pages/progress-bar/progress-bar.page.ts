import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.page.html',
  styleUrls: ['./progress-bar.page.scss'],
})
export class ProgressBarPage implements OnInit {

  porcentage = 0.05;

  constructor() { }

  ngOnInit() {
  }

  rangeChange(e) {
    this.porcentage = e.detail.value / 100;
  }
}
