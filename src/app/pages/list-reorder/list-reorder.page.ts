import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-reorder',
  templateUrl: './list-reorder.page.html',
  styleUrls: ['./list-reorder.page.scss'],
})
export class ListReorderPage implements OnInit {

  characters = ['Aquaman', 'Superman', 'Batman', 'Flash', 'Wonder Woman'];

  constructor() { }

  ngOnInit() {
  }

  reorder(e) {
    e.detail.complete(this.characters);
  }

  onClick() {
    console.log(this.characters);
  }

}
