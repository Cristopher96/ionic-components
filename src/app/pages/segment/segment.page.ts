import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {

  @ViewChild(IonSegment, {static: true}) segment: IonSegment;
  heroes: Observable<any>;
  publisher = '';

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.segment.value = 'All';
    this.heroes = this.dataService.getHeroes();
  }

  segmentChanged(e) {
    const segmentValue = e.detail.value;
    if (segmentValue === 'All') {
      this.publisher = '';
      return;
    }
    this.publisher = segmentValue;
  }
}
