import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.page.html',
  styleUrls: ['./date-time.page.scss'],
})
export class DateTimePage implements OnInit {

  fechaNaci: Date = new Date();
  customPickerOptions;
  customDate;

  constructor() { }

  ngOnInit() {
    this.customPickerOptions = {
      buttons: [{
        text: 'Save',
        handler: (e) => {
          console.log('Clicked save!');
          console.log(e);
        }
      }, {
        text: 'Log',
        handler: () => {
          console.log('Clicked log. do not dismiss');
          return false;
        }
      }]
    };
  }

  cambioFecha(e) {
    console.log('ionChange', e);
    console.log('Date', new Date(e.detail.value));
  }

}
