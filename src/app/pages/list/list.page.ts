import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { IonList, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  usuarios: Observable<any>;

  @ViewChild('list', {static: false}) list: IonList;

  constructor(
    private dataService: DataService,
    private toastCtrl: ToastController
    ) { }

  ngOnInit() {
    this.usuarios = this.dataService.getUsers();
  }

  async presentToast(m: string) {
    const toast = await this.toastCtrl.create({
      message: m,
      duration: 2000
    });
    toast.present();
  }

  favorite(u) {
    this.presentToast('Saved in Favorite');
    this.list.closeSlidingItems();
  }

  share(u){
    this.presentToast('Shared');
    this.list.closeSlidingItems();
  }

  delete(u) {
    this.presentToast('Deleted');
    this.list.closeSlidingItems();
  }

}
